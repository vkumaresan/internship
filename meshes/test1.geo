// Gmsh project created on Fri Aug 28 09:54:11 2020
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {20, 0, 0, 1.0};
//+
Point(3) = {20, 5, 0, 1.0};
//+
Point(4) = {0, 5, 0, 1.0};
//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 4};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Curve("Inlet") = {4};
//+
Physical Curve("Outlet") = {2};
//+
Physical Curve("Walls") = {1, 3};
//+
Physical Surface("fluid") = {1};
