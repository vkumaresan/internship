// Gmsh project created on Mon Aug 31 11:03:44 2020
//+
SetFactory("OpenCASCADE");
//+
SetFactory("Built-in");
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {10, 0, 0, 1.0};
//+
Point(3) = {10, 25, 0, 1.0};
//+
Point(4) = {30, 25, 0, 1.0};
//+
Point(5) = {30, 10, 0, 1.0};
//+
Point(6) = {20, 10, 0, 1.0};
//+
Point(7) = {10, 10, 0, 1.0};
//+
Point(8) = {0, 25, 0, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 7};
//+
Line(3) = {7, 5};
//+
Line(4) = {5, 4};
//+
Line(5) = {4, 8};
//+
Line(6) = {8, 1};
//+
Curve Loop(1) = {5, 6, 1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Plane Surface(2) = {1};
//+
Physical Curve("Inlet") = {1};
//+
Physical Curve("Outlet") = {4};
//+
Physical Curve("Walls") = {2, 3, 5, 6};
//+
Plane Surface(3) = {1};
//+
Physical Surface("fluid") = {1};
