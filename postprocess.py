# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 10:24:20 2020

@author: Vishanth
"""

from nutils import util, export
import treelog
import numpy as np
from matplotlib import pyplot as plt, collections

def post(lhs, topo, geom, ns, cons):
    bezier = topo.sample('bezier', 5)
    x, p, u, divu = bezier.eval(['x_i', 'p', 'u_i', 'd(u_i, x_i)'] @ ns, lhs=lhs)
    
    # bbox = np.array([[-2,46/9],[-2,2]]) # bounding box for figure based on 16x9 aspect ratio
    # bezier0 = topo.sample('bezier', 5)
    # bezier = bezier0.subset((bezier0.eval((ns.x-bbox[:,0]) * (bbox[:,1]-ns.x)) > 0).all(axis=1))
    # interpolate = util.tri_interpolator(bezier.tri, bezier.eval(ns.x), mergetol=1e-5) # interpolator for quivers
    # spacing = .05 # initial quiver spacing
    # xgrd = util.regularize(bbox, spacing)
    # ugrd = interpolate[xgrd](u)
    
# #velocity plot
#     with export.mplfigure('velocity.png', dpi=400) as fig:
#         ax = fig.add_subplot(1, 1, 1, aspect=1)
#         ax.set(xlabel='$x_1$', ylabel='$x_2$', title='Speed')
#         im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, np.linalg.norm(u, axis=-1), shading='gouraud', cmap='jet')
#         ax.add_collection(collections.LineCollection(x[bezier.hull], colors='k', linewidths=0.5, alpha=0.3))
#         ax.autoscale(enable=True, axis='both', tight=True)
#         fig.colorbar(im, label='$|\mathbf{u}|$')

# # pressure plot
#     with export.mplfigure('pressure.png', dpi=400) as fig:
#         ax = fig.add_subplot(1, 1, 1, aspect=1)
#         ax.set(xlabel='$x_1$', ylabel='$x_2$', title='Pressure')
#         im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, p, shading='gouraud', cmap='jet')
#         ax.add_collection(collections.LineCollection(x[bezier.hull], colors='k', linewidths=0.5, alpha=0.3))
#         ax.autoscale(enable=True, axis='both', tight=True)
#         fig.colorbar(im, label='$p$')

# # divergence plot
#     with export.mplfigure('udiv.png', dpi=400) as fig:
#         ax = fig.add_subplot(1, 1, 1, aspect=1)
#         ax.set(xlabel='$x_1$', ylabel='$x_2$', title='div')
#         im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, divu, shading='gouraud', cmap='jet')
#         ax.add_collection(collections.LineCollection(x[bezier.hull], colors='k', linewidths=0.5, alpha=0.3))
#         ax.autoscale(enable=True, axis='both', tight=True)
#         fig.colorbar(im, label='$udiv$')
    

# MATPLOTLIB

#velocity plot
    fig, ax = plt.subplots(figsize = (16,8))
    ax.set(xlabel = '$x_1$', ylabel = '$x_2$', title = 'Speed')
    im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, np.linalg.norm(u, axis = -1), shading = 'gouraud', cmap = 'jet')
    ax.add_collection(collections.LineCollection(x[bezier.hull], colors = 'k', linewidths = 0.5, alpha = 0.3))
    #ax.quiver(x[:,0], x[:,1], u[:,0], u[:,1], scale = 0.5, width = 0.001)
    ax.autoscale(enable = True, axis = 'both', tight = True)
    fig.colorbar(im, label = '$|\mathbf{u}|$')
    plt.savefig('plots/velocity.png', dpi = 400)
    
#quiver plot
    fig, ax = plt.subplots(figsize = (16,8))
    ax.set(xlabel = '$x_1$', ylabel = '$x_2$', title = 'quiver')
    ax.quiver(x[:,0], x[:,1], u[:,0], u[:,1], scale = 0.5, width = 0.001)
    # ax.plot(x[:,0], x[:,1], np.isnan(cons))
    plt.savefig('plots/quiver.png', dpi = 400)
    
# pressure plot
    fig, ax = plt.subplots(figsize = (16,8))
    ax.set(xlabel='$x_1$', ylabel='$x_2$', title='Pressure')
    im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, p, shading='gouraud', cmap='jet')
    ax.add_collection(collections.LineCollection(x[bezier.hull], colors='k', linewidths=0.5, alpha=0.3))
    ax.autoscale(enable=True, axis='both', tight=True)
    fig.colorbar(im, label='$p$')
    plt.savefig('plots/pressure.png', dpi = 400)
    
def debug(lhs, topo, geom, ns, cons):
    bezier = topo.sample('bezier', 5)
    x, p, u, divu = bezier.eval(['x_i', 'p', 'u_i', 'd(u_i, x_i)'] @ ns, lhs=np.isnan(cons))
    
    #velocity plot
    fig, ax = plt.subplots(figsize = (16,8))
    ax.set(xlabel = '$x_1$', ylabel = '$x_2$', title = 'Speed')
    im = ax.tripcolor(x[:, 0], x[:, 1], bezier.tri, np.linalg.norm(u, axis = -1), shading = 'gouraud', cmap = 'jet')
    ax.add_collection(collections.LineCollection(x[bezier.hull], colors = 'k', linewidths = 0.5, alpha = 0.3))
    ax.quiver(x[:,0], x[:,1], u[:,0], u[:,1], scale = 0.5, width = 0.001)
    ax.autoscale(enable = True, axis = 'both', tight = True)
    fig.colorbar(im, label = '$|\mathbf{u}|$')
    plt.savefig('plots/velocity.png', dpi = 400)
    
    

