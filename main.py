# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 09:20:40 2020

@author: Vishanth
"""

import import_mesh, namespace, esolver, postprocess

#Creating mesh

#choice = input("Enter your choice \n 1) Import gmsh \n 2) Use default mesh \n")
choice = '2'
topo,geom = import_mesh.meshgen(choice)
if choice == '2':
    topo = topo.withboundary(
        Inlet = topo['patch0'].boundary['bottom'],
        Out1 = topo['patch3'].boundary['right'], 
        Out2 = topo['patch4'].boundary['left, bottom, right']) 
    topo = topo.withboundary(
        Outlet = topo.boundary['Out1'] + topo.boundary['Out2'],
        Walls = topo.boundary - topo.boundary['Inlet, Outlet'])
print("\n MESH LOADED SUCCESSFULLY")

#Creating namespace

ns = namespace.name(geom, topo)

#solving the flow equation

print("\n SOLVING ...\n")
lhs, cons = esolver.sol(ns, topo, geom)
print("\n SOLVED !!")

#plotting

print("\n PLOTTING \n")
postprocess.post(lhs, topo, geom, ns, cons)