# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 10:06:44 2020

@author: Vishanth
"""

from nutils import solver

def sol(ns, topo, geom):
    #idegree = int(input("\n Enter the value for degree : "))
    idegree = 5
    res = topo.integral('mu d(ubasis_ni, x_j) d(u_i, x_j) J(x)' @ ns, degree = idegree)
    res += topo.integral('pbasis_n d(u_i, x_i) J(x)' @ ns, degree = idegree)
    res -= topo.integral('p d(ubasis_ni, x_i) J(x)' @ ns, degree = idegree)
    
    sqr = topo.boundary['Walls'].integral('u_i u_i J(x)' @ ns, degree = idegree)
    sqr += topo.boundary['Inlet'].integral('((u_1 - uin)^2 + (u_0 u_0)) J(x)' @ ns, degree = idegree)
    cons = solver.optimize('lhs', sqr, droptol=1e-12)
    lhs = solver.solve_linear('lhs', res, constrain=cons)
    return lhs, cons