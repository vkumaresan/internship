# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 09:18:51 2020

@author: Vishanth
"""

from nutils import mesh

def meshgen(inp):
    if inp == '1':
        name = input("Enter the name of the gmsh file with '.msh' extension : ")
        folder = 'meshes/'
        ret = mesh.gmsh(folder + name)
        return ret
    elif inp == '2':
        #ret = mesh.multipatch(patches=[[0,1,3,4], [1,2,4,5], [4,5,6,7]],patchverts=[[0,0], [0,5], [0,10], [5,0], [5,5], [5,10], [30,5], [30,10]],nelems={None: 8, (4,6): 16, (5,7): 16})
        ret = mesh.multipatch(patches=[[0,1,3,4], [1,2,4,5], [4,5,6,7],[6,7,8,9],[6,10,8,11]],patchverts=[[0,0], [0,2], [0,4], [2,0], [2,2], [2,4], [30,2], [30,4], [31,2],[31,4],[30,1.5],[31,1.5]],nelems={None: 8, (4,6): 16, (5,7): 16})
        return ret
    else:
        print("Enter a valid input")
        
            