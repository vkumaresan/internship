# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 09:46:43 2020

@author: Vishanth
"""

from nutils import function

def name(geom, topo):
    #visc = float(input("\n Enter the value of viscosity : "))
    #vel = float(input("\n Enter the value of input velocity : "))
    #udegree = int(input("\n Enter the value of u-basis function : "))
    udegree = 2
    visc = 1.85e-5
    vel = 0.16
    ns = function.Namespace()
    ns.x = geom
    ns.mu = visc
    ns.uin = vel
    ns.ubasis, ns.pbasis = function.chain([
        topo.basis('std', degree = udegree).vector(topo.ndims),
        topo.basis('std', degree = udegree - 1)])
    ns.u_i = 'ubasis_ni ?lhs_n'
    ns.p = 'pbasis_n ?lhs_n'
    ns.sigma_ij = '(d(u_i, x_j) + d(u_j, x_i)) mu - p δ_ij'
    return ns
    